osslsigncode (2.5-4) unstable; urgency=medium

  * Import upstream fixes for big-endian systems. Closes: #993452.
  * Import upstream fixes for pkcs12 signing. Closes: #1024434.

 -- Stephen Kitt <skitt@debian.org>  Sun, 22 Jan 2023 23:15:10 +0100

osslsigncode (2.5-3) unstable; urgency=medium

  * Switch to the OpenSSL variant of libcurl; since osslsigncode as an
    OpenSSL exception, this is fine and reduces the overall dependency
    tree.

 -- Stephen Kitt <skitt@debian.org>  Sat, 19 Nov 2022 16:15:26 +0100

osslsigncode (2.5-2) unstable; urgency=medium

  * Re-run failed tests verbosely on failure.

 -- Stephen Kitt <skitt@debian.org>  Wed, 31 Aug 2022 20:41:48 +0200

osslsigncode (2.5-1) unstable; urgency=medium

  * New upstream release.

 -- Stephen Kitt <skitt@debian.org>  Sun, 14 Aug 2022 14:56:39 +0200

osslsigncode (2.4-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version 4.6.1, no change required.

 -- Stephen Kitt <skitt@debian.org>  Fri, 12 Aug 2022 15:33:33 +0200

osslsigncode (2.3.0-1) unstable; urgency=medium

  * Rework debian/watch to download release tarballs and verify their
    signature.
  * New upstream release.
  * The test suite available in the upstream repository is not yet
    intended for general consumption and is not part of the release
    tarballs, so debian/rules no longer tries to run them.
    Closes: #993451, #993452.

 -- Stephen Kitt <skitt@debian.org>  Wed, 09 Mar 2022 16:27:11 +0100

osslsigncode (2.2-1) unstable; urgency=medium

  * New upstream release.
  * Run the provided test suite during the build.
  * Standards-Version 4.6.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Wed, 01 Sep 2021 16:18:02 +0200

osslsigncode (2.1-1) unstable; urgency=low

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Stephen Kitt ]
  * New upstream release.
  * Switch to debhelper compatibility level 13.
  * Standards-Version 4.5.0, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Thu, 15 Oct 2020 09:08:44 +0200

osslsigncode (2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Stephen Kitt ]
  * New upstream release.
  * Switch to Michał Trojnara’s fork.
  * Set “Rules-Requires-Root: no”.
  * Standards-Version 4.2.1, no change required.

 -- Stephen Kitt <skitt@debian.org>  Thu, 06 Dec 2018 09:46:12 +0100

osslsigncode (1.7.1-3) unstable; urgency=medium

  * Actually fix debian/watch.
  * Build-depend on libcurl4-gnutls-dev to avoid build errors once
    libcurl4-openssl-dev switches to OpenSSL 1.1; thanks to Steve Langasek
    for the investigation and fix! Closes: #891781.
  * Switch to debhelper compatibility level 11.
  * Update debian/copyright.
  * Standards-Version 4.1.3, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Fri, 02 Mar 2018 21:21:48 +0100

osslsigncode (1.7.1-2) unstable; urgency=medium

  * Switch to https: VCS URIs (see #810378).
  * Build-depend on libssl1.0-dev, converting to OpenSSL 1.1 is going to
    involve a lot of work... Closes: #828483.
  * Switch to debhelper compatibility level 10.
  * Clean up debian/control using cme.
  * Standards-Version 3.9.8, no change required.
  * Clean up debian/rules and debian/copyright.
  * Fix debian/watch.
  * Enable all hardening options.

 -- Stephen Kitt <skitt@debian.org>  Sat, 05 Nov 2016 17:02:20 +0100

osslsigncode (1.7.1-1) unstable; urgency=low

  * Initial release. Closes: #796916.

 -- Stephen Kitt <skitt@debian.org>  Tue, 25 Aug 2015 21:59:09 +0200
